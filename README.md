# Bing - Doorbell


- Web interface is an aurelia app showing images and temperatures
- App is a python app running in a systemd service

## App

### Components
- RPI
- RPI Cam
- LED Button (momentary push button switch)
- 3.3v -> 5.v converter (to control the 5v led on the button)
- DS18B20 Thermometer Dallas TO-92

1. edit bing.cfg
2. edit bing.service
3. copy bing.service to /lib/systemd/system/bing.service
4. `systemctl start bing`
5. `systemctl enable bing`




![Bing1](https://gitlab.com/hwestman/bing/raw/master/IMG_1529.jpg)
![Bing2](https://gitlab.com/hwestman/bing/raw/master/IMG_4475.JPG)
![Bing3](https://gitlab.com/hwestman/bing/raw/master/IMG_4525.JPG)

## Schematics (If i'm allowed to call it that, clearly i'm not an electrical engineer)
![Bing4](https://gitlab.com/hwestman/bing/raw/master/bing_schematic.png)