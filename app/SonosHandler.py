#!/usr/bin/env python

import os
from soco import SoCo
import logging
import time,datetime
import threading
from soco.snapshot import Snapshot
from Helper import ReportFinished
import ConfigParser
from NotifyHandler import RegisterBing, FirePushBullet

config = ConfigParser.RawConfigParser()
CWD = os.path.dirname(os.path.abspath(__file__))
config.read(CWD+'/bing.cfg')

def BDay(theDay):
    	if theDay.day == 21 and theDay.month == 8 or theDay.day == 10 and theDay.month == 4 or theDay.day == 1 and theDay.month == 1:
        	return True
    	return False

class PlaySonos(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        try:
            threading.Thread.__init__(self, group=group, target=target, name=name,
                                    verbose=verbose)
            self._stop = threading.Event()
            
            self.bingEvent = kwargs['bingEvent']
            self.clearEvent = kwargs['clearEvent']
            self.queue = kwargs['queue']
            self.lock = kwargs['lock']

            self.players = GetPlayers()
            self.kitchen = self.players['kitchen']
            self.living = self.players['livingRoom']
            logging.info("Playsonos initiated")  
        except Exception as ex:
            logging.exception("Playsonos could not initiate: "+ str(ex.message))
            raise  
    
    
    def run(self):
        logging.info("Playsonos is running")  
        while(self.stopped() != True):

            bingHappend = self.bingEvent.wait()            
            try:
                grouped = False
                
                #testing purposes
                # file = config.get('main', 'bingSound')
                # kitchen.play_uri(file)

                #check if speakers are grouped
                if len(self.kitchen.group.members) > 1 or len(self.living.group.members) > 1:
                        grouped = True

                snapshots = TakeSnapshots(self.players)
                file = ConfigurePlayers(self.players)
                
                #play
                self.living.play_uri(file)
                state = self.living.get_current_transport_info()['current_transport_state']
                limit = 0

                #try:
                #    FirePushBullet(path,config.get('main', 'pushbulletAPIKey'))
                #except Exception as ex:
                #    logging.exception("could not fire pushbullet"+ str(ex.message))
                
                while(state != 'STOPPED'):
                       time.sleep(1)
                       state = self.living.get_current_transport_info()['current_transport_state']                    

                if not grouped:
                    kitchen.unjoin()
                    self.players['kitchen'].unjoin()

                #RestoreSnapshots(snapshots)
            except Exception as ex:
                logging.error("Problem playing bing on sonos: "+ex.message)

            ReportFinished("sonosThread",self.bingEvent,self.clearEvent,self.queue,self.lock)

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()    
#class end----------------------------------------------


def GetPlayers():
    logging.info("Entered getplayers")
    #for living in soco.discover():
    #    if living.player_name == 'Living Room':
    #        break
    #    else:
    #        living = None

    #for kitchen in soco.discover():
    #    if kitchen.player_name == 'Kitchen':
    #        break
    #    else:
    #        kitchen = None
    kitchen = SoCo('10.0.0.227')
    logging.info(kitchen.player_name)
    living = SoCo('10.0.0.226')   
    return {'kitchen':kitchen,'livingRoom':living}



def ConfigurePlayers(players):
    players['kitchen'].join(players['livingRoom'])
    vol = 15
    file = config.get('main', 'bingSound')
    today = datetime.datetime.now()
    if today.hour >= 18 or today.hour <= 9:
            vol = 10

    SetPlayerVol(players,vol)

    if BDay(today) == True:
        file = config.get('main', 'bdaySound')
    return file

def TakeSnapshots(players):
    snapShots = []
    for k, v in players.items():
        curSnap = Snapshot(v)
        curSnap.snapshot()
        snapShots.append(curSnap)
        
    return snapShots

def SetPlayerVol(players,vol):
    for k, v in players.items():
        v.volume = vol

def RestoreSnapshots(snapshots):
    
    for snapshot in snapshots:
        snapshot.restore()

