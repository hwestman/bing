#!/usr/bin/env python
import time
import logging
import RPi.GPIO as GPIO

def ReportFinished(threadName,bingEvent,clearEvent,queue,lock):
    
    lock.acquire()    
    
    counter = queue.get()
    limit = queue.get()
    counter += 1 #counting this instance
    logging.info(threadName+" finished, count: "+str(counter))
    if counter == limit:        
        queue.put(0)
        queue.put(limit)
        lock.release()
        bingEvent.clear()
        clearEvent.set()

    else:
        queue.put(counter)
        queue.put(limit)        
        lock.release()
        clearEvent.wait()


def SignalLed(led):
    for x in range(1,10):
		GPIO.output(led,GPIO.LOW)
		time.sleep(0.1)
		GPIO.output(led,GPIO.HIGH)
		time.sleep(0.1)


def InitiateGPIO(button, led):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(led, GPIO.OUT)
    GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.output(led, GPIO.HIGH)
