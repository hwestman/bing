#!/usr/bin/env python
import os
import threading
import Queue
import logging
import ConfigParser
import time
import sys
import signal
import RPi.GPIO as GPIO
from Helper import SignalLed,InitiateGPIO,ReportFinished

CWD = os.path.dirname(os.path.abspath(__file__))
config = ConfigParser.RawConfigParser()
config.read(CWD+'/bing.cfg')

logLevel = logging.getLevelName(config.get('main', 'loglevel'))
logging.basicConfig(filename=CWD+'/'+config.get('main', 'logfilename'),
                    level=logLevel,
                    format='%(asctime)s %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S',
                    stream=sys.stdout)

logging.info("Got config and logging going")

try:
    from ThreadHandler import CreateThreads, StartThreads, JoinThreads
except Exception as ex:
    logging.exception("Could not import")
    quit()

#GLOBALS
global LED,BUTTON,SIGNAL,bingEvent,clearEvent,lock,q
LED = int(config.get('main', 'led'))
BUTTON = int(config.get('main', 'button'))
SIGNAL = False
#creating shared resources
bingEvent = threading.Event()
clearEvent = threading.Event()
lock = threading.Lock()
q = Queue.Queue()


#----------------------------------------------------------
def RunButtonPressLoop():
    global SIGNAL,BUTTON

    while True:
        try:
            input_state = GPIO.input(BUTTON)
            if input_state == False or SIGNAL == True:

                if(SIGNAL == True):
                    logging.info("Bing happened by SIGNAL")
                    SIGNAL = False
                else:
                    logging.info("Bing happened by button")
                ButtonPress()

        except Exception as inst:
            logging.exception("Exception in loop")
        time.sleep(0.1)
#----------------------------------------------------------
def ButtonPress():
    global LED,bingEvent,clearEvent,lock,q
    bingEvent.set()
    SignalLed(LED)
    ReportFinished("mainthread",bingEvent,clearEvent,q,lock)

#----------------------------------------------------------
def SignalInput():
    global SIGNAL
    while True:
        if(SIGNAL == True):
            ButtonPress()
            SIGNAL = False
        time.sleep(0.1)

#----------------------------------------------------------
def ManualInput():
    cmd = ''
    
    while (cmd != 'q'):
        cmd = raw_input('Cmd: ')
        if(cmd != 's'):
            bingEvent.set()            

#Run sudo kill -14 <processid> to manually trigger
#----------------------------------------------------------
def RSignal(signum, stack):
    global SIGNAL
    SIGNAL = True    


#----------------------------------------------------------
def WritePid():
    #pid = os.getpid()
    #pid = psutil.Process(os.getpid()).ppid()
    pid = os.getpid()
    f = open('bing.pid', 'w')
    f.write(str(pid))
    f.close()


#---------------------------MAIN-------------------------------
if __name__ == "__main__":
    logging.info("Started bing")
    
    try:
        WritePid()

        
        q.put(0) #0(threadsFinishedCount) should be first to come out in a lock
        signal.signal(signal.SIGALRM, RSignal)

        #create and threads
        threads = CreateThreads(bingEvent,clearEvent,q,lock)
        q.put(len(threads)+1) #threadsCount is the secondvariable in the queue, they need to stick this order + 1 is for main thread
        StartThreads(threads)

        InitiateGPIO(BUTTON,LED)

        #Running main loop, listen for buttonpress
        logging.info("Initate A-OK running loop..")
        RunButtonPressLoop()
        #ManualInput()
        #SignalInput()

        #ending operaion
        JoinThreads(threads)
    except Exception as ex:
        logging.exception("Exception in main")
