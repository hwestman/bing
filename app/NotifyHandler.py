#!/usr/bin/env python
import requests
#from pushbullet.pushbullet import PushBullet
from Helper import ReportFinished
import os
import logging
from time import gmtime, strftime, localtime
import ConfigParser

config = ConfigParser.RawConfigParser()
CWD = os.path.dirname(os.path.abspath(__file__))
config.read(CWD+'/bing.cfg')


def FirePushBullet(picturePath,pushBulletAPIKey):

    #curTime = strftime("%Y-%m-%d %H:%M:%S", localtime())
    
    
    try:
        curHour = strftime("%H:%M:%S", localtime())
        pb = PushBullet(pushBulletAPIKey)
        devices = pb.getDevices()
        pb.pushFile(config.get('main', 'pushbulletChannel'),
                    "bing.jpg","Bing!: %s"%curHour,open(picturePath,
                    "rb"),recipient_type='channel_tag')
                    
    except Exception as ex:
        logging.exception("could not pushbullet: ")




def RegisterBing(image):
    try:
        files = {'file': image}
        r = requests.post(config.get('main', 'registerBingURI'),files=files)
        

    except Exception as inst:
        logging.exception("could post to bing api")
        

    

    
