import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(15, GPIO.OUT)

GPIO.output(15, GPIO.HIGH)
time.sleep(2)
GPIO.output(15, GPIO.LOW)
time.sleep(2)
GPIO.output(15, GPIO.HIGH)
