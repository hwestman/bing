#!/usr/bin/env python

import logging
import datetime
import threading
import os
import time
from PIL import Image
from Helper import ReportFinished
#from picamera import PiCamera
import ConfigParser
from NotifyHandler import RegisterBing #, FirePushBullet
import operator
import requests
import base64
from io import BytesIO

import ConfigParser
config = ConfigParser.RawConfigParser()
CWD = os.path.dirname(os.path.abspath(__file__))
config.read(CWD+'/bing.cfg')

image_path = config.get('main', 'picturePath')

class TakePicture(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        try:
            threading.Thread.__init__(self, group=group, target=target, name=name,
                                    verbose=verbose)
            self._stop = threading.Event()
            
            self.bingEvent = kwargs['bingEvent']
            self.clearEvent = kwargs['clearEvent']
            self.queue = kwargs['queue']
            self.lock = kwargs['lock']
        
            logging.info("takepicture initiated")  

        except Exception as ex:            
            logging.exception("Could not initiate takepicture: ")
    
    def run(self):
        logging.info("running takepicture")
        while(self.stopped() != True):
            bingHappend = self.bingEvent.wait()
            
            try:                
                imageAnalyzed = self.GetAndAnalyzeImage()
                #RegisterBing(imageAnalyzed['image'])
                self.NotifyHass(imageAnalyzed['title'],imageAnalyzed['message'])
                
            except Exception as ex:
                logging.exception("Problem taking picture: ")
                #logging.exception("could not pushbullet")    
            
            ReportFinished("TakePictueThread",self.bingEvent,self.clearEvent,self.queue,self.lock)
    
    def emotion(self,emotion):
        if emotion == 'anger':
            return 'angry'
        if emotion == 'contempt':
            return 'filled with contempt'
        if emotion == 'disgust':
            return 'distugsted'
        if emotion == 'fear':
            return 'afraid'
        if emotion == 'happiness':
            return 'happy'
        if emotion == 'neutral':
            return 'neutral'
        if emotion == 'sadness':
            return 'sad'
        if emotion == 'suprise':
            return 'suprised'

    def maxEmotion(self,emotions):
        return self.emotion(max(emotions.iteritems(),key=operator.itemgetter(1))[0])

    def maxHair(self,haircolors):
        if len(haircolors) <= 0:
            return ""
        
        mostConfident = haircolors[0]
        for color in haircolors:
            if color['confidence'] > mostConfident['confidence']:
                mostConfident = color

        return "{} hair".format(mostConfident['color'])
    

    def TakePicture(self):
        
        url = config.get('main', 'image-url') #"http://localhost:8080/stream/snapshot.jpeg?delay_s=0"
        response = requests.get(url)      

        image = Image.open(BytesIO(response.content))  
        buffer = BytesIO()
        image.save(buffer,format="JPEG")
        
        return buffer.getvalue()
        
    def VisionAnalysis(self,image):
        vision_subscription_key = config.get('main', 'ms-vision-api-key')
        assert vision_subscription_key

        vision_base_url = "https://northeurope.api.cognitive.microsoft.com/vision/v2.0/"
        vision_analyze_url = vision_base_url + "analyze"

        vision_headers = {'Ocp-Apim-Subscription-Key': vision_subscription_key ,'Content-Type': 'application/octet-stream'}
        vision_params  = {'visualFeatures': 'Categories,Description,Color'}        

        response = requests.post(vision_analyze_url, headers=vision_headers, params=vision_params, data=image)
        response.raise_for_status()
        return response.json()


    def FaceAnalysis(self,image):
        subscription_key = config.get('main', 'ms-face-api-key')
        assert subscription_key

        face_api_url = 'https://northeurope.api.cognitive.microsoft.com/face/v1.0/detect'

        msheaders = {'Ocp-Apim-Subscription-Key': subscription_key,'Content-Type': 'application/octet-stream'}
        params = {
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,' +
            'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'
        }
        response = requests.post(face_api_url, params=params, headers=msheaders, data=image)
        return response.json()

    def GetAndAnalyzeImage(self):
        
        image = self.TakePicture()
        faces = self.FaceAnalysis(image)
        limit = 0

        #while len(faces) < 1 and limit < 3:
        #    logging.info("Could not find any faces, sleep 0.{} and try again".format(limit))
        #    time.sleep(float("0.{}".format(limit)))

        #    image = self.TakePicture()
        #    faces = self.FaceAnalysis(image)
        #    limit += 1

        #if limit == 3 and len(faces) == 0:
        #    logging.warn("No faces detected and limit was met")
        
        logging.info(faces)

        vision = self.VisionAnalysis(image)
        logging.info(vision)

        title = "Bing({})".format(len(faces))
        imageDescription = ""
        
        if len(vision['description']['captions']) > 0:
            imageDescription = vision['description']['captions'][0]['text']
        

        message = ""
        if len(imageDescription) > 50:    
            message += "{}\n".format(imageDescription)
        else:
            title += " {}".format(imageDescription)
        
        celebrities = {}
        for x in vision['categories']:
            if  'detail' in x:
                if 'celebrities' in x['detail']:
                    celebrities = x['detail']['celebrities']
                    
            if len(celebrities) > 0:
                break

        message += self.BuildFacesMessage(faces,celebrities)
        
        if len(message) < 1:
            message = "No face analysis available"
        logging.info(title)
        logging.info(message)

        return {'title':title,'message':message,'image':image}
        
    
    def BuildFacesMessage(self,faces,celebrities):

        message = ""            

        for face in faces:
            message += face['faceAttributes']['gender']
            message += "({})".format(int(face['faceAttributes']['age']))

            hair = self.maxHair(face['faceAttributes']['hair']['hairColor'])
            if(len(hair) > 0):
                message += ", {}".format(hair)
            
            curEmotion = self.maxEmotion(face['faceAttributes']['emotion'])
            if len(curEmotion) > 0:
                message += ", {}".format(curEmotion)
            
            if float(face['faceAttributes']['smile']) > 0.5:
                message += ", smiling"
                
            if faces[0]['faceAttributes']['glasses'] != 'NoGlasses':
                message += ", wearing glasses"
             
            if len(celebrities) > 0:
                for celebritie in celebrities:
                    if celebritie['faceRectangle']['top'] == face['faceRectangle']['top'] and celebritie['faceRectangle']['left'] == face['faceRectangle']['left']:
                        message += ", kinda looks like {}".format(celebritie['name'])
                
            message += '\n'
            

        return message

    def NotifyHass(self,title,message):
        
        url = 'https://ha.j-burg11.dev/api/services/script/notify_doorbell'
        hassheaders = {'Authorization': "Bearer "+config.get('main', 'hassAccessToken'),'content-type': 'application/json'}
        data={"title":title,"message":message}
        response = requests.post(url, headers=hassheaders,json=data)
    
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
