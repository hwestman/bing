#!/usr/bin/env python
#from PhueHandler import SignalPhue
#from SonosHandler import PlaySonos
from PictureHandler import TakePicture
from HASSHandler import NotifyHASS


import threading
def CreateThreads(bingEvent,clearEvent,queue,lock):

    threads = []
    events = []

    #sonosThread = PlaySonos(kwargs={'bingEvent':bingEvent,'clearEvent':clearEvent,'queue':queue,'lock':lock})
    #sonosThread.setDaemon(True)


    #takePictureThread = TakePicture(kwargs={'bingEvent':bingEvent,'clearEvent':clearEvent,'queue':queue,'lock':lock})
    #takePictureThread.setDaemon(True)

    #phueThread = SignalPhue(kwargs={'bingEvent':bingEvent,'clearEvent':clearEvent,'queue':queue,'lock':lock})
    #phueThread.setDaemon(True)

    notifyhassThread = NotifyHASS(kwargs={'bingEvent':bingEvent,'clearEvent':clearEvent,'queue':queue,'lock':lock})
    notifyhassThread.setDaemon(True)

    threads.append(notifyhassThread)
    #threads.append(takePictureThread)
    #threads.append(phueThread)

    return threads

def StartThreads(threads):
    for thread in threads:
        thread.start()

def JoinThreads(threads):
     for thread in threads:
        thread.stop()
        thread.join()

