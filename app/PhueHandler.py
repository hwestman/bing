#!/usr/bin/env python
import time
from phue import Bridge
import threading
from Helper import ReportFinished
import os
import logging
import ConfigParser
config = ConfigParser.RawConfigParser()

CWD = os.path.dirname(os.path.abspath(__file__))
config.read(CWD+'/bing.cfg')

class SignalPhue(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        try:
            threading.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
            self._stop = threading.Event()
            self.bingEvent = kwargs['bingEvent']
            self.clearEvent = kwargs['clearEvent']
            self.queue = kwargs['queue']
            self.lock = kwargs['lock']
            self.lights = None
            self.lights = Bridge(config.get('main', 'phueBridgeIP'))
            self.lights.connect()
            self.lights.get_api()
            logging.info("SignalPhue initiated")
        except Exception as ex:
            self.stop()
            logging.exception("SignalPhue could not initiate: "+ex.message)

    def run(self):
        if(self.stopped() == True):
            logging.error("SignalPhue could not start running")
            return
        logging.info("SignalPhue is running")

        while self.stopped() != True:
            bingHappend = self.bingEvent.wait()
            try:
                self.BlinkLights()
            except Exception as ex:
                logging.exception("could not perform blinklights: "+ex.message)

            ReportFinished("phueThread",self.bingEvent,self.clearEvent,self.queue,self.lock)

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    
    def BlinkLights(self):

        if(self.lights.get_light(1, 'on')):
            initialBrightnes = self.lights.get_light(1, 'bri')
            self.lights.set_light(1, 'bri', int(initialBrightnes * 0.5),transitiontime=2)   
            time.sleep(0.3)
            self.lights.set_light(1, 'bri', initialBrightnes,transitiontime=2)         
        else:
            self.lights.set_light(1,'on', True)
            time.sleep(0.3)
            self.lights.set_light(1,'on', False)
        

