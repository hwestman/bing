#!/usr/bin/env python
import time
import threading
import requests
from Helper import ReportFinished
import os
import logging
import ConfigParser
config = ConfigParser.RawConfigParser()

CWD = os.path.dirname(os.path.abspath(__file__))
config.read(CWD+'/bing.cfg')


class NotifyHASS(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        try:
            threading.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
            self._stop = threading.Event()
            self.bingEvent = kwargs['bingEvent']
            self.clearEvent = kwargs['clearEvent']
            self.queue = kwargs['queue']
            self.lock = kwargs['lock']
           
            logging.info("NotifyHASS initiated")
        except Exception as ex:
            self.stop()
            logging.exception("NotifyHASS could not initiate: "+ex.message)
   

    def run(self):
        if(self.stopped() == True):
            logging.error("NotifyHASS could not start running")
            return
        logging.info("NotifyHASS is running")

        while self.stopped() != True:
            bingHappend = self.bingEvent.wait()
            try:                
                self.NotifyHASS()
            except Exception as ex:
                logging.exception("could not perform notifyhass: "+ex.message)

            ReportFinished("notifyhassThread",self.bingEvent,self.clearEvent,self.queue,self.lock)

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
        
    def NotifyHASS(self):
        #logging.info("running notifyhass function") 
        url = 'https://ha.j-burg11.dev/api/services/script/ring_doorbell'
        hassheaders = {'Authorization': "Bearer "+config.get('main', 'hassAccessToken'),'content-type': 'application/json'}
        
        response = requests.post(url, headers=hassheaders)
        logging.debug(response.text)        
