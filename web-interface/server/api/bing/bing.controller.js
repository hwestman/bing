var Bing = require('../../bing.js');
var path = require('path');
exports.registerBing = function(req,res){
    var bing = new Bing();

    if(req.files != null){
        var bingImage = req.files.file;
        bing.img = Math.floor(Date.now() / 1000) + '.png';

        var mvTo  = __dirname + '/../../bing-images/' + bing.img;

        bingImage.mv(mvTo, function(err) {
            if (err) {
                console.log(err);
                console.log("could not save image");
                //res.status(500).send(err);
            }
            else {
                console.log("image was saved");
                //res.send('File uploaded!');                
            }
        });
    }
    
    bing.save(function(err){
        if(err){
            res.status(500).send({error:err});
        }else{
            res.json({status:200});
        }
    });

}
exports.getBing = function(req,res){

    Bing.find({}).sort({date: 'desc'}).exec(function(err, bings) {
        res.json(bings);
    });

}
exports.getLatestBingImage = function(req,res){

    Bing.find({}).sort({date: 'desc'}).limit(1).exec(function(err, bing) {
        console.log("err\n");
        console.log(err);
        console.log("bing\n");
        console.log(bing);
        res.sendFile(path.resolve(__dirname + '/../../bing-images/'+ bing[0].img));
    });

}