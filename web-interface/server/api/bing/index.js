var express = require('express');
var bingController = require('./bing.controller');
var router = express.Router();

//router.get('/', tempController.getTemp );

router.get('/latestbingimage', bingController.getLatestBingImage);
router.get('/', bingController.getBing);
router.post('/register', bingController.registerBing);

module.exports = router;