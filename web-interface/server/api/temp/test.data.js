exports.getTestData = function(){


    var now = new Date();
    var minDate = new Date().setMinutes(now.getMinutes()-20);
    var maxDate = new Date().setMinutes(now.getMinutes()-10);



    resObj = {
        min:{"value":"-4","_id":"57aa42546fbf4eab6439f76a","date":minDate},
        max:{"value":"11","_id":"57aa42546fbf4eab6439f76b","date":maxDate},
        cur:{"value":"0","_id":"57aa42546fbf4eab6439f76c","date":now},
        recent:[{"value":"1","_id":"57aa42546fbf4eab6439f76d","date":new Date().setHours(1)}
                ,{"value":"-4","_id":"57aa42546fbf4eab6439f76e","date":new Date().setHours(4)}
                ,{"value":"3","_id":"57aa42546fbf4eab6439f76f","date":new Date().setHours(7)}
                ,{"value":"11","_id":"57aa42546fbf4eab6439f76g","date":new Date().setHours(10)}
                ,{"value":"5","_id":"57aa42546fbf4eab6439f76h","date":new Date().setHours(13)}
                ,{"value":"8","_id":"57aa42546fbf4eab6439f76i","date":new Date().setHours(15)}
                ,{"value":"8","_id":"57aa42546fbf4eab6439f76i","date":new Date().setHours(18)}
                ,{"value":"10","_id":"57aa42546fbf4eab6439f76i","date":new Date().setHours(21)}                
               ]
        }
    
    return resObj;
}