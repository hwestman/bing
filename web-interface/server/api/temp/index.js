var express = require('express');
var tempController = require('./temp.controller');
var router = express.Router();

//router.get('/', tempController.getTemp );
router.get('/', tempController.getReading);
router.get('/register', tempController.registerReading);
router.get('/primereadings', tempController.GetPrimaryReadingsByDay);
router.post('/toggleknit', tempController.ToggleKnit);

module.exports = router;