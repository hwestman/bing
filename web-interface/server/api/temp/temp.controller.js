'use strict';
var async = require('async');
var Sensor = require('../../sensors.js');
var Reading = require('../../reading.js');
var serverConfig = require('../../config.js').serverConfig;
var testData = require('./test.data.js').getTestData();


var getReadingData =  function () {

    var cutoff = new Date().setHours(0,0,0,0);
    //cutoff.setDate(cutoff.getDate()-1);
    
    var resObj = {
            min:null,
            max:null,
            cur:null,
            recent:null            
        }
    
    var p1 = new Promise(function(resolve,reject){
        if(serverConfig.environment == 'dev'){
            resolve(testData);
            return
        }
        Reading.find({date: {$gt: cutoff}}).sort({date: 'desc'}).exec(function(err, readings) {
            
            if(!readings || readings.length<=0){
                console.log("no readings");
                reject();   
            }

            var min = readings[0];
            var max = readings[0];
            var currentRead = readings[0];

            for(var i = 0;i < readings.length;i++){
                
                if(Number(readings[i].value) < Number(min.value)){
                    min = readings[i];
                }
                if(Number(readings[i].value) > Number(max.value)){
                    max = readings[i];
                }
            }

            resObj.min = min;
            resObj.max = max;
            resObj.cur = currentRead;
            resObj.recent = readings;

            resolve(resObj);
            
        })
        .catch(function(err){
            console.log('error:', err);
            reject(err);            
        });
    });

    return p1;
}



var getReading = function (req, res) {

    getReadingData().then(function(data){
        if(data==null){
            return res.status(400);    
        }
        return res.status(200).json(data);
    }).catch(function(err){
        console.log(err);
        return res.status(400).send({message: err});   
    });
    
}
var GetPrimaryReadingsByDay = function (req, res) {

    var days = getDates(new Date("2018-01-01T11:55:00.000Z"),new Date());
    
    

    let readings = [];
    async.eachSeries(days, function (day, callback) {        
        Reading.findOne(
        {
            date: 
            {
                $gte: day,
                $lt: new Date(day.getTime() + 10*60000)
            }
        },function(err,reading){
            readings.push(reading);
            callback(err)
        });;            
    }, function (err) {
        if (err) throw err;

        return res.status(200).json({ readings});
    });

    return res.status(500);
}
Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

var registerReading = function(req,res){

    var reading = new Reading();
    reading.id = req.query.desc;
    reading.value = req.query.value;

    reading.save(function(err){
        if(err){
            res.status(500).send({error:err});
        }else{
            res.json({status:200});
        }
    });
}
var ToggleKnit = function(req,res){
    
    //console.log(req.body.daysKnitted);
    var daysKnittedIds = req.body.daysKnitted.map(a => a._id);
    var daysNotKnittedIds = req.body.daysNotKnitted.map(a => a._id);
    //console.log(daysKnittedIds);
    UpdateKnitted(daysKnittedIds,true)
        .then(UpdateKnitted(daysNotKnittedIds,false))    
        .then(function () {
            res.status(200).send({});
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send({error:err});
        });

}

const UpdateKnitted = function (ids,value) {
    return new Promise(function (resolve, reject) {
        if(ids.length == 0){
            return resolve();
        }
        Reading.find({ _id: { '$in': ids } },(err,readings)=>{
            console.log(err);
            if (err) return reject(err);
            
            async.forEachOf(readings, function (reading, callback) {     
                
                reading.knitted = value;
                reading.save();

            }, function (err) {
                console.log(err);
                return reject(err);                
            });
        
            return resolve();
            
        });

    });
}

module.exports = {
  getReadingData: getReadingData,
  getReading: getReading,
  registerReading:registerReading,
  GetPrimaryReadingsByDay:GetPrimaryReadingsByDay,
  ToggleKnit:ToggleKnit
}

