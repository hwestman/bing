var mongoose = require('mongoose');

module.exports = mongoose.model('Sensor', {
	name: String,
	id: String,
	readings:[{
        value:String,
        date:{type:Date,default:Date.now}
    }]
});
