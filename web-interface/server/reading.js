var mongoose = require('mongoose');

module.exports = mongoose.model('Reading', {
	id:String,
    value:String,
    date:{type:Date,default:Date.now},
    knitted:Boolean    
});
