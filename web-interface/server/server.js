var express = require('express');
var app = express();
var mongoose = require('mongoose');
var server = require('http').Server(app);
var io = require('socket.io')(server);

var fileUpload = require('express-fileupload');

var cron = require('node-cron');
var bodyParser = require('body-parser');
var jenkinsapi = require('jenkins-api');
var statusController = require('./controllers/status-controller.js')
var tempController = require('./api/temp/temp.controller.js')
var bSocket = null;
var serverConfig = require('./config.js').serverConfig
//var sensorController = require('./controllers/sensor-controller.js')


//var connectionString = 'mongodb://wsw-test.westman.no:27017/sensor'


mongoose.connect(serverConfig.connectionString)

//app.use(express.static(__dirname+'../client'));
// app.use(express.static('/Users/hwestman/git/bing/web-interface/client/'));
//console.log(__dirname + '../leg-client');


app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(express.static(__dirname + '/../leg-client'));
app.use('/strikk',express.static(__dirname + '/../strikk/Strikk'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use('/bing-images', express.static(__dirname + '/bing-images'));

app.use('/api/bing/register',function(req, res, next){
    if(bSocket != null){
        bSocket.emit('bing','Recieved a BING');
        bSocket.broadcast.emit('bing','Recieved a BING');
    }            
    next();
});
require('./routes')(app);

app.post('/api/getStatus', statusController.getStatus);
//app.get('/api/sensor',sensorController.PostTempReading);
//app.get('/api/register',sensorController.RegisterReading);
//app.get('/api/import',sensorController.ImportReadings);

var port = '1338';

server.listen(port, function () {

    // io.on('connection', function (socket) {
    //     if(bSocket == null){
    //         bSocket = socket;
    //     }
        
    //     cron.schedule('* */5 * * * *', function () {

    //         tempController.getReadingData().then(function(res){
    //             socket.emit('temp', res);
    //         }).catch(function(err){
    //             console.log(err);
    //         });
            
    //     });
    // });
    console.log('this is working on port:' + port);
});