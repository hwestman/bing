var jenkinsapi=require('jenkins-api')
module.exports.getStatus = function(req,res){

	var jenkins = jenkinsapi.init("http://hwestman:0745d04a3a024c023f0f67aabe46208d@centralperk.no-ip.org:8080/") 

	jenkins.all_jobs_in_view('dashboard', function(err, data) {
      if(err){
			console.log(err);
			res.json({status:401});
		}
		
		var compositJobs = [];
		var jobRequests = 0;


		data.forEach(function (job, i) {
			jobRequests++;
			jenkins.last_build_info(job.name, function(err, buildData) {
			  jobRequests--;
			  if (err){ return console.log(err); }
			  

			  compositJobs.push({info:job,
			  					last_build_info:buildData});

			  if(jobRequests == 0){
			  	done();
			  }
			  
			});
		    
		});

		function done(){
			//console.log(compositJobs);

			var sort_by = function(field, reverse, primer){

				   var key = primer ? 
				       function(x) {return primer(x.info[field])} : 
				       function(x) {return x.info[field]};

				   reverse = !reverse ? 1 : -1;

				   return function (a, b) {
				       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
				     } 
				}

				//compositJobs.sort(compare);
				compositJobs.sort(sort_by('name', false, function(a){return a.toUpperCase()}));


			res.json({status:200,
				   data:compositJobs});	
		}
		
		
	});
}
