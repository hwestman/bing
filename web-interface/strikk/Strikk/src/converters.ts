import * as moment from 'moment';
import * as yarn from './YarnModel';

export class DateFormatValueConverter {
	toView(value) {
		return moment(value).format('D. MMM');
	}
}
export class TempFormatValueConverter {
	toView(value) {
		return Math.round( value * 10 ) / 10;
	}
}
export class YarnValueConverter {
	toView(value,nameOrType) {
    var res = yarn.Yarn.filter(temp => value >= temp.tempFrom && value < temp.tempTo);

    if(res.length != 1){
      console.log("This aint right, multiple values for yarnconverter");
      throw "To many yarnvalues";
    }

    if(nameOrType == 'name'){
      return res[0].name;
    }else if(nameOrType == 'type'){
      return res[0].type;
    }else{
      throw "Invalid parameter"+nameOrType;
    }
    
	}
}

export class ReverseValueConverter {  
  toView(array) {    
    if(array){
      return array.slice().reverse();
    }    
  }
}

