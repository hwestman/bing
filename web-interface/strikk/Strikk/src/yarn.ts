import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import * as yarn from './YarnModel';

@inject(HttpClient,json)
export class Yarn {
  http;
  yarn;
  constructor(http) {
    this.http = http;
    this.yarn = yarn.Yarn.reverse();
  }
    
  activate(){
    
  }
  
}