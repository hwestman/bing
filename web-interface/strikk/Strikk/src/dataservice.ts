import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';


@inject(HttpClient,json)
export class DataService {
  http;
  days;
  constructor(http) {
    this.http = http;
    	
  }
  getDays(){

      if(this.days){
        return Promise.resolve(this.days);
      }else{
        return this.http.fetch('/api/temp/primereadings')
        .then(response => response.json())
        .then(data => {
            
            this.days = data.readings;
            return Promise.resolve(this.days);
        });
      }
  }
}