export let Yarn = [    
    {name:'Svart',type:'svart',tempFrom:-100,tempTo:-25},
    {name:'Petrol',type:'petrol',tempFrom:-25,tempTo:-20},            
    {name:'Turkis/Grå',type:'turkis-gra',tempFrom:-20,tempTo:-15},
    {name:'Grågrønn',type:'gragronn',tempFrom:-15,tempTo:-10},
    {name:'Lys grågrønn',type:'lys-gragronn',tempFrom:-10,tempTo:-5},
    {name:'Natur',type:'natur',tempFrom:-5,tempTo:0},
    {name:'Lys camel',type:'lys-camel',tempFrom:0,tempTo:5},
    {name:'Oker',type:'oker',tempFrom:5,tempTo:10},
    {name:'Orangemelert',type:'orangemelert',tempFrom:10,tempTo:15},
    {name:'Vinrød',type:'vinrod',tempFrom:15,tempTo:20},
    {name:'Tomatrød',type:'tomatrod',tempFrom:20,tempTo:25},                        
    {name:'Rød/lilla',type:'rod-lilla',tempFrom:25,tempTo:100}    
]; 