import {Router, RouterConfiguration} from 'aurelia-router';
import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';


@inject(HttpClient,json)
export class App {
  router: Router;  
  http;
  days;
  constructor(http) {
    this.http = http;
	
  }
  activate(){    
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Strikk';

    config.map([
      { route: '',          moduleId: 'knit',   title: 'Strikk',    name: "index",    nav: true},     
      { route: 'yarn',     moduleId: 'yarn',   title: 'Garn' ,    name: "yarn",    nav: true},            
    ]);

    this.router = router;
  }

}

