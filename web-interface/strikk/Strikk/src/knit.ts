import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import {DataService} from './dataservice';

@inject(HttpClient,DataService,json)
export class Knit {
  http;
  days;
  originalDays;  
  dataservice;
  loading;
  constructor(http,dataservice) {
    this.http = http;
    this.dataservice = dataservice;    
    this.loading = true;
    
  }
  activate(){
    this.dataservice.getDays()
    .then(days => {
        this.days = days;
        this.loading = false;
    });
  }

  Toggle(day){
    let index = this.days.indexOf(day);
    let daysKnitted = this.days.slice(0,index+1);
    let daysNotKnitted = this.days.slice(index == 0 ? index : index+1,this.days.length);

    if(daysKnitted.length == 1){            
      daysKnitted[0].knitted = !daysKnitted[0].knitted;            
    }else{
      daysKnitted.forEach((x) => {
          x.knitted = true;
      });
    }

    daysNotKnitted.forEach((x) => {
      x.knitted = false;
    });
    
    this.http.fetch('/api/temp/toggleknit', {
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
      },
      body: JSON.stringify({'daysKnitted':daysKnitted,'daysNotKnitted':daysNotKnitted})
    })
    .then(response => {
      console.log(response);
      if (response.status !== 200) {
        day.knitted = !day.knitted;
        response.text().then(text => {
          console.log(text)
        });        

      }
      
    });
    
  }
}
