'use strict';

System.register(['aurelia-framework', 'aurelia-fetch-client'], function (_export, _context) {
    "use strict";

    var inject, HttpClient, json, _dec, _class, Bing;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    return {
        setters: [function (_aureliaFramework) {
            inject = _aureliaFramework.inject;
        }, function (_aureliaFetchClient) {
            HttpClient = _aureliaFetchClient.HttpClient;
            json = _aureliaFetchClient.json;
        }],
        execute: function () {
            _export('Bing', Bing = (_dec = inject(HttpClient, json), _dec(_class = function () {
                function Bing(http) {
                    _classCallCheck(this, Bing);

                    this.http = http;
                    this.url = 'api/temp/';
                }

                Bing.prototype.activate = function activate() {
                    var model = this;

                    model.http.fetch(model.url).then(function (response) {
                        return response.json();
                    }).then(function (data) {

                        console.log(data);
                        model.data = data;
                    });
                };

                return Bing;
            }()) || _class));

            _export('Bing', Bing);
        }
    };
});
//# sourceMappingURL=bing.js.map
