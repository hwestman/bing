'use strict';

System.register(['bootstrap', 'font-awesome', 'whatwg-fetch'], function (_export, _context) {
	"use strict";

	function configure(aurelia) {
		aurelia.use.standardConfiguration().developmentLogging();

		aurelia.start().then(function (a) {
			return a.setRoot("shell");
		});
	}

	_export('configure', configure);

	return {
		setters: [function (_bootstrap) {}, function (_fontAwesome) {}, function (_whatwgFetch) {}],
		execute: function () {}
	};
});
//# sourceMappingURL=main.js.map
