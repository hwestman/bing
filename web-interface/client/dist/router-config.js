'use strict';

System.register(['aurelia-framework', 'aurelia-router'], function (_export, _context) {
	"use strict";

	var inject, Router, _dec, _class, _default;

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	return {
		setters: [function (_aureliaFramework) {
			inject = _aureliaFramework.inject;
		}, function (_aureliaRouter) {
			Router = _aureliaRouter.Router;
		}],
		execute: function () {
			_export('default', _default = (_dec = inject(Router), _dec(_class = function () {
				function _default(router) {
					_classCallCheck(this, _default);

					this.router = router;
				}

				_default.prototype.configure = function configure() {
					var appRouterConfig = function appRouterConfig(config) {
						config.title = 'Folk';

						config.map([{ route: ['', 'bing'], moduleId: './bing/bing', nav: true, title: 'Bing' }]);
					};

					this.router.configure(appRouterConfig);
				};

				return _default;
			}()) || _class));

			_export('default', _default);
		}
	};
});
//# sourceMappingURL=router-config.js.map
