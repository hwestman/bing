'use strict';

System.register(['moment'], function (_export, _context) {
  "use strict";

  var moment, DateFormatValueConverter, DateFormatTodayValueConverter, RoundValueConverter;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  return {
    setters: [function (_moment) {
      moment = _moment.default;
    }],
    execute: function () {
      _export('DateFormatValueConverter', DateFormatValueConverter = function () {
        function DateFormatValueConverter() {
          _classCallCheck(this, DateFormatValueConverter);
        }

        DateFormatValueConverter.prototype.toView = function toView(value) {
          return moment(value).format('HH:mm - D.MMM');
        };

        return DateFormatValueConverter;
      }());

      _export('DateFormatValueConverter', DateFormatValueConverter);

      _export('DateFormatTodayValueConverter', DateFormatTodayValueConverter = function () {
        function DateFormatTodayValueConverter() {
          _classCallCheck(this, DateFormatTodayValueConverter);
        }

        DateFormatTodayValueConverter.prototype.toView = function toView(value) {
          return moment(value).format('HH:mm');
        };

        return DateFormatTodayValueConverter;
      }());

      _export('DateFormatTodayValueConverter', DateFormatTodayValueConverter);

      _export('RoundValueConverter', RoundValueConverter = function () {
        function RoundValueConverter() {
          _classCallCheck(this, RoundValueConverter);
        }

        RoundValueConverter.prototype.toView = function toView(value) {
          return Math.round(value);
        };

        return RoundValueConverter;
      }());

      _export('RoundValueConverter', RoundValueConverter);
    }
  };
});
//# sourceMappingURL=converters.js.map
