'use strict';

System.register(['aurelia-framework', 'aurelia-router', './router-config'], function (_export, _context) {
    "use strict";

    var inject, Router, AppRouterConfig, _dec, _class, Shell;

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    return {
        setters: [function (_aureliaFramework) {
            inject = _aureliaFramework.inject;
        }, function (_aureliaRouter) {
            Router = _aureliaRouter.Router;
        }, function (_routerConfig) {
            AppRouterConfig = _routerConfig.default;
        }],
        execute: function () {
            _export('Shell', Shell = (_dec = inject(Router, AppRouterConfig), _dec(_class = function () {
                function Shell(router, appRouterConfig) {
                    _classCallCheck(this, Shell);

                    this.router = router;
                    this.appRouterConfig = appRouterConfig;

                    this.email = "hallvard@westman.no";
                }

                Shell.prototype.activate = function activate() {
                    this.appRouterConfig.configure();
                };

                return Shell;
            }()) || _class));

            _export('Shell', Shell);
        }
    };
});
//# sourceMappingURL=shell.js.map
