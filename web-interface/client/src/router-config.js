
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

@inject(Router)
export default class{

	constructor(router){
		this.router = router;
	}
	configure(){
		var appRouterConfig = function(config){
			config.title = 'Folk';
			
			config.map([
				{ route: ['','bing'],    moduleId: './bing/bing',      	nav: true,  	title:'Bing' },                		
				]);
			};

		this.router.configure(appRouterConfig);	
	}

}
