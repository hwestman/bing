import {inject} from 'aurelia-framework';
//import {CustomHttpClient} from '../auth/custom-http-client';
import {HttpClient,json} from 'aurelia-fetch-client';
//@inject(CustomHttpClient)
@inject(HttpClient,json)

export class Bing{
    
    constructor(http){
        this.http = http;    
        this.url = 'api/temp/';
    };
    activate(){
        var model = this;
        //setInterval(function(){
            model.http.fetch(model.url)
                .then(response => response.json())
                .then(data => {
                    
                    console.log(data);
                    model.data = data;
            });
        //}, 1000);

        // return this.http.fetch(this.url)
        //         .then(response =>  response.json())
        //         .then(c => this.user = c);
        
    }
    
}
