import 'bootstrap';
import 'font-awesome';
import 'whatwg-fetch';

export function configure(aurelia){
	aurelia.use
	.standardConfiguration()
	.developmentLogging();

	aurelia.start().then(a=>a.setRoot("shell"));
}