
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import AppRouterConfig from './router-config';

@inject(Router,AppRouterConfig)

export class Shell{
	constructor(router, appRouterConfig){
        this.router = router;
        this.appRouterConfig = appRouterConfig;
        
        
		this.email = "hallvard@westman.no";
	}	
    activate(){
        this.appRouterConfig.configure();
        
        
    }
    
}