import moment from 'moment';

export class DateFormatValueConverter {
  toView(value) {
    return moment(value).format('HH:mm - D.MMM');
  }
}

export class DateFormatTodayValueConverter {
  toView(value) {
    return moment(value).format('HH:mm');
  }
}

export class RoundValueConverter {
  toView(value) {
    return Math.round(value);
  }
}