
var curData = null; 
var lastUpdated = Date.now();
var lastBing = null;



function getTempColor(temp){
    if(temp>0){
        return '#ff7772';
    }else{
        return '#5bc0de';
    }
}

function updateView(data){
    if(data != null){
        $("#current-temp").html(Math.round(data.cur.value)+'°');
        $("#current-temp").css('color',getTempColor(data.cur.value));

        //$("#highest-temp-date").html(moment(data.max.date).format('HH:mm'));
        $("#highest-temp").html(Math.round(data.max.value)+'°<span class="badge">'+moment(data.max.date).format('HH:mm')+'</span>');
        $("#highest-temp").css('color',getTempColor(data.max.value));

        $("#lowest-temp").html(Math.round(data.min.value)+'°<span class="badge">'+moment(data.min.date).format('HH:mm')+'</span>');
        //$("#lowest-temp-date").html(moment(data.min.date).format('HH:mm'));
        $("#lowest-temp").css('color',getTempColor(data.min.value));
    }
    
    // var ctx = document.getElementById("myChart");


    // var recentTemps = [];
    // var recentLabels = [];
    // for(i=0;i<data.recent.length;i++){
    //     recentTemps.push(data.recent[i].value);
    //     recentLabels.push(moment(data.recent[i].date).format('HH:mm'))
    // }
    // //I have flipping the sorted array above here, this is all stupid. todo: make it not stupid
    // recentTemps.reverse();
    // recentLabels.reverse();

    // var gdata = {
    // labels: recentLabels,
    // datasets: [
    //     {
    //         label: "Temperature",
    //         fill: false,
    //         lineTension: 0.9,
    //         backgroundColor: "000",
    //         borderColor: "#222",
    //         borderCapStyle: 'butt',
    //         borderDash: [],
    //         borderDashOffset: 0.0,
    //         borderJoinStyle: 'miter',
    //         pointBorderColor: "#222",
    //         pointBackgroundColor: "#fff",
    //         pointBorderWidth: 1,
    //         pointRadius: 1,
    //         pointHitRadius: 10,
    //         data: recentTemps,
    //         spanGaps: false,
    //     }
    // ]
    // };

    

    // var myLineChart = new Chart(ctx, {
    //     type: 'line',
    //     data: gdata,
    //     options: {
    //         legend: {
    //         display: false
    //         },
    //     scales: {
    //         xAxes: [{
    //             display: false
    //         }],
    //         yAxes: [{
    //             display: false
    //         }],
    //     }
    // }
    // });

}

function getLastPush(){
    $.ajax({
        type: "GET",
        url: clientConfig.host+"/api/bing/"
        
    }).done(function(data) {
        
        $("#bing-img").attr('src','bing-images/'+data[0].img);
        $("#bing-created").html(moment(data[0].date).fromNow());

        lastBing = moment(data[0].date);
        
    });
}
$( document ).ready(function() {
    toastr.options.preventDuplicates = true;
    getLastPush();
    
    //Getting initial data
    $.get( "/api/temp/", function( data ) {
        
        curData = data;
        lastUpdated = Date.now();
        updateView(data);                  
    }).fail(function(res){ 
        toastr.error('There are no current readings, is the pi doing its job?','', {timeOut: 0});
    });
    
    //setting up socket to recieve updates from server
    var socket = io(clientConfig.host);
    socket.on('temp', function (data) {            
        
        if(Date.now()-lastUpdated > 120000){
            curData = data;
            lastUpdated = Date.now();

            updateView(data);
        }

            
    });
    socket.on('bing', function (data) {            
        getLastPush();
         
    });
    //Checking that data is updated
    setInterval(function(){
        var now = Date.now();
        if(moment(curData.cur.date).diff(now,'minutes') < -10){
            
            toastr.warning('Data is outdatet', moment(now).format('MMMM Do, HH:mm:ss'), {timeOut: 0});
            $("#last-updated-container").removeClass("label-success");
            $("#last-updated-container").addClass("label-warning");
        }else{
            if($("#last-updated-container").hasClass("label-warning")){
                $("#last-updated-container").removeClass("label-warning");
                $("#last-updated-container").addClass("label-success");
            }
        } 
        
    }, 600000);//10 min interval

    $("#last-updated").html(moment(lastUpdated).fromNow());
    $("#last-updated-timestamp").html('('+moment(lastUpdated).format('MMMM Do, HH:mm:ss')+')');
    setInterval(function(){
         $("#last-updated").html(moment(lastUpdated).fromNow());
         $("#last-updated-timestamp").html('('+moment(lastUpdated).format('MMMM Do, HH:mm:ss')+')');
         $("#bing-created").html(moment(lastBing).fromNow());         
    }, 61000);

    

});